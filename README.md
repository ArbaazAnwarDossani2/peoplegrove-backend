Since we decided to develop a web application that works on 3 layers i.e. Database (PostGreSql) , Middleware (NodeJS) and frontend(ReactJS => Didn't have enough time to explore Angular), I am elaborating my project as 3 separate components:

1. Database Layer : 
I am generally familiar with No-Sql databases, but upon your suggestion in the assignment, I went on to explore Postgres as well. According to the requirement of the application, I made three tables in the database namely pg_admins, pg_user_masters and pg_task_masters. I connected my Node app with the database with a very well known node module - Sequelize.
The database was deployed locally (couldn't find time for remote database deployment) with following configuration:
const sequelize = new Sequelize('postgres://arbaazdossani:people123@localhost:5432/peoplegrove');

2. Middleware Layer (NodeJS) :

The following routes were created for NodeJS application:

TYPE     ROUTE                              DESCRIPTION
POST     /login                       User Login Route called after response from FB SDK          
POST     /admin                       Admin Login Route to verify admin by email
POST     /addTask                     Add a Task with content, startTime and endTime
GET       /getAllTasks                Get All Tasks for Admin to view
GET       /getTask/:taskId            Get a particular Task            
POST     /editTask/:taskId            Edit a particular Task
GET       /getAllUserTasks            Get Tasks of a particular User

In nodeJS I am using express, Sequelize and express-promise-router which makes it simple to handle promises and structure the code. 
The app was deployed locally on port 8080 and deployed on Heroku(free Instance) on the following Url:
https://people-grovebe.herokuapp.com/


3. The FrontEnd Layer (ReactJS w/ REDUX)

The React Application serves as the FrontEnd where Redux is used for State-Management with advanced in Redux-Saga and Redux-Persist.
Axios is used for API requests.​
For login via Facebook, I have used the standard Facebook SDK with redirect URIs of both local and remote system.

Since I took up Frontend at the eventual, I missed completion of some part of it like :
a. Admin Panel => Admin Login and GetTask APIs ready. Display part can be easily reused from the User side.
b. Calender Integration => The application currently cannot segregate tasks by Date nor can it disable editing on past dates. I have however used moment which can easily format date time like '1 hour', etc

The React FrontEnd application was developed locally at port 3000 using create-react-app (ejected Later) and deployed to Heroku(free instance) on the following URL:
https://people-herokufe.herokuapp.com

I have used Ant Design as the UI toolkit with react as it provides vast components and also integrated with GrammarOfGraphics(GOG) if needed 
​
Please find the procedure to start the application on your local system:

NODEJS
1. Clone the repository : https://ArbaazAnwarDossani2@bitbucket.org/ArbaazAnwarDossani2/peoplegrove-backend.git
2. Install the node_modules: npm install
3. Since we are running on our local system:: npm run local (Runs a nodemon instance which can be restarted any moment on port 8080)

REACTJS
1. Clone the repository: https://ArbaazAnwarDossani2@bitbucket.org/ArbaazAnwarDossani2/people-grove-frontend.git
2. Install the node_modules: npm install
3. Since we are running on our local system:: npm run local (Runs a node script instance hot reloading at port 3000)