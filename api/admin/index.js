const router = require('express-promise-router')();
const controller = require('./controller');
const Middleware = require('../middleware');

router.post('/login',
  controller.login,
  Middleware.sendResponse
);

module.exports = router;
