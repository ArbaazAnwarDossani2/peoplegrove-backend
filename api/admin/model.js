const Sequelize = require('sequelize');
const config = require("../../config/environment");
const sequelize = new Sequelize(config.pg.uri);

const Admin = sequelize.define('pg_admin', {
    email: {
      type: sequelize.Sequelize.STRING
    }
  },
  {
    timestamps: true
  }
);

module.exports = {
  login: (data, cb) => {
    return Admin.sync().then(() => {
        Admin.find({
        where:{
            email: data.email
        }
        }).then((res,err) => {
            cb(res);
        })
      })
    },
}
