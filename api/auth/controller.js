const User = require('./model');

exports.login = (req, res, next) => {
  const data = req.body;
  User.login(data, (res)=> {
    if(res){
      req.data = {
        meta: res,
        status: 200
      }
      return next()
    }
    else{
      req.data = {
        meta: 'User does not exist',
        status: 500
      }
      return next()
    }
  })
};
