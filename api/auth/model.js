const Sequelize = require('sequelize');
const config = require("../../config/environment");
const sequelize = new Sequelize(config.pg.uri);

const User = sequelize.define('pg_user_master', {
    user_id: {
      type: sequelize.Sequelize.STRING
    },
    name: {
      type: sequelize.Sequelize.STRING
    },
    email: {
      type: sequelize.Sequelize.STRING
    },
    gender: {
      type: sequelize.Sequelize.STRING
    },
  },
  {
    timestamps: true
  }
);

module.exports = {
  login: (data, cb) => {
    return User.sync().then(() => {
        User.findOrCreate({
        where:{
            user_id: data.user_id
        },
        defaults: {
          gender: data.gender,
          email: data.email,
          name: data.name
        }
        }).then((res,err) => {
            cb(res)
        })
      })
    },
}
