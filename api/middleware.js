module.exports = {
  sendResponse: (req, res) => res.status(200).send(req.data)
};
