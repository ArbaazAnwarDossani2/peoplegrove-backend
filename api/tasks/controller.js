const Task = require('./model');

exports.initial = (req, res, next) => {
  req.data = "Initial App Setup";
  return next()
};

exports.addTask = (req, res, next) => {
  let task = req.body;
  Task.addTask(req.body).then((res) => {
    if(res.dataValues){
      req.data = {
        meta: 'Task Added Successfully',
        status: 200
      }
      return next()
    }
    else{
      req.data = {
        meta: 'Unable to Add task',
        status: 500
      }
      return next()
    }
  })
};


exports.getAllTasks = (req, res, next) => {
  Task.getAllTasks((res) => {
    if(res.length != 0){
      req.data = {
        meta: res,
        status: 200
      }
      return next()
    }
    else{
      req.data = {
        meta: 'Unable to Fetch Tasks',
        status: 500
      }
      return next()
    }

  })
};

exports.getTask = (req, res, next) => {
  const id = req.params.id;

  Task.getTask(id, (res) => {
    if(res.length != 0){
      req.data = {
        meta: res,
        status: 200
      }
      return next()
    }
    else{
      req.data = {
        meta: 'Unable to Fetch Task',
        status: 500
      }
      return next()
    }

  })
};

exports.getUserTasks = (req, res, next) => {
  const id = req.params.id;

  Task.getUserTasks(id, (res) => {
    if(res.length != 0){
      req.data = {
        meta: res,
        status: 200
      }
      return next()
    }
    else{
      req.data = {
        meta: 'Unable to Fetch Tasks',
        status: 500
      }
      return next()
    }

  })
};

exports.editTask = (req, res, next) => {
  const id = req.params.id;
  let task = req.body;
  Object.keys(task).map(function(key, index) {
    let updateObject={};
    updateObject[key] = task[key];
    Task.updateTask(id, updateObject, (res) => {
      if(Object.keys(task).length - 1 === index){
        if(res.length != 0){
          req.data = {
            meta: "Task Edited Successfully",
            status: 200
          }
          return next()
        }
        else{
          req.data = {
            meta: 'Unable to Edit Task',
            status: 500
          }
          return next()
        }
      }
    });
  })
};
