const router = require('express-promise-router')();
const controller = require('./controller');
const Middleware = require('../middleware');

router.post('/addTask',
  controller.addTask,
  Middleware.sendResponse
);

router.get('/getAllTasks',
  controller.getAllTasks,
  Middleware.sendResponse
);

router.get('/getTask/:id*',
  controller.getTask,
  Middleware.sendResponse
);

router.post('/editTask/:id*',
  controller.editTask,
  Middleware.sendResponse
);

router.get('/getUserTasks/:id*',
  controller.getUserTasks,
  Middleware.sendResponse
);

module.exports = router;
