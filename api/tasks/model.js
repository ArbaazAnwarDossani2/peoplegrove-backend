const Sequelize = require('sequelize');
const config = require("../../config/environment");
const sequelize = new Sequelize(config.pg.uri);

const Task = sequelize.define('pg_task_master', {
    task_id: {
      type: sequelize.Sequelize.STRING
    },
    content: {
      type: sequelize.Sequelize.STRING
    },
    user_id: {
      type: sequelize.Sequelize.STRING
    },
    start_date: {
      type: 'TIMESTAMP',
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
    end_date: {
      type: 'TIMESTAMP',
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
  },
  {
    timestamps: true
  }
);


module.exports = {
  addTask: (data) => {
    return Task.sync().then(() => {
      return Task.create(data);
    });
  },

  getAllTasks: (cb) => {
    return Task.findAll().then((res,err) => {
      cb(res);
    })
  },

  getTask: (id, cb) => {
    return Task.findAll({ where: { task_id: id } }).then((res,err) => {
      cb(res);
    })
  },

  getUserTasks: (id, cb) => {
    return Task.findAll({ where: { user_id: id } }).then((res,err) => {
      cb(res);
    })
  },

  updateTask: (id, request, cb) => {
    return Task.update(request,{ where: { task_id: id } }).then((res,err) => {
      cb(res);
    })
  },
}
