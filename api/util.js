const Bluebird = require('bluebird');
const jwt = require('jsonwebtoken');

const config = require('../config/environment');

exports.getJwtToken = (data) => new Bluebird((resolve, reject) => {
  return jwt.sign(data, config.JWT_SECRET_KEY, {}, (err, token) => (err ? reject(err) : resolve(token)))
});
