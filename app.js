const express = require("express");
const Sequelize = require("sequelize");

const config = require("./config/environment");
const sequelize = new Sequelize(config.pg.uri);
// sequelize
//   .authenticate()
//   .then(() => {
//     console.log('Connection has been established successfully.');
//   })
//   .catch(err => {
//     console.error('Unable to connect to the database:', err);
//   });

const app = express();
const server = require('http').createServer(app);

require('./config/express')(app);
require('./routes')(app);

server.listen(config.port, () => {
  console.log('Spiritual app listening on port 8080!')
});

module.exports = server;
