const _ = require('lodash');

const envConfig = require(`./${process.env.NODE_ENV || 'development'}.js`);

const all = {

  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 8080,
  mongo: {
    options: {
      db: {
        safe: true,
      },
    },
  },

  JWT_SECRET_KEY: 'spiritual@2017$app&aa',

  PASSWORD_SECRET_KEY: 'spiritual@app&aa',

};

module.exports = _.merge(all, envConfig);
