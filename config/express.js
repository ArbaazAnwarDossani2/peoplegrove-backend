const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const cors = require('cors')

const config = require('./environment');

module.exports = (app) => {
  app.use(cookieParser(config.JWT_SECRET_KEY));
  app.use(bodyParser.urlencoded({extended: true, limit: '50mb', parameterLimit:50000}));
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(cors({
    origin: (origin, callback) => {
      // if (config.env !== 'production') {
      //   return callback(null, true);
      // }
      if (origin) {
        origin = origin.slice(origin.indexOf('://') + 3);
        return callback(null, config.whitelist.indexOf(origin) !== -1);
      }
      return callback(null, true);
    },
    credentials: true,
  }));
  app.disable('etag');
  app.use(morgan('dev'));
};
