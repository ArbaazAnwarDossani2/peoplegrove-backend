const admin = require('./api/admin');
const task = require('./api/tasks');
const user = require('./api/auth');

module.exports = (app) => {
    app.use('/user', user);
    app.use('/admin', admin);
    app.use('/tasks', task);
};
